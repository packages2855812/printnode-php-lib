<?php

namespace D3x\PrintNode;
use d3x\starter\StarterServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class PrintNodeServiceProvider extends ServiceProvider
{
    public function boot(){

        $this->publishes([
            __DIR__ . '/config/printnode.php' => config_path('printnode.php'),
        ]);
        $this->publishes([
            __DIR__ . '/routes/api.php' => base_path('routes/printnode.php'),
        ], "printnode-routes");
    }


    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/printnode.php', 'printnode'
        );
    }

}