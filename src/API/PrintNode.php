<?php

namespace D3x\PrintNode\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;


class PrintNode
{
    private static $client;

    public static function getClient()
    {
        if (!self::$client) {
            self::$client = new Client([
                "base_uri" => config("printnode.base_url"),
                "headers" => [
                    "Authorization" => "Basic ". base64_encode(config("printnode.api_key"))
                ]
            ]);
        }
        return self::$client;
    }

    public static function get($endpoint, $params = [])
    {
            $response = self::getClient()->request('GET', $endpoint);
            return json_decode($response->getBody()->getContents(), true);
    }

    public static function post($endpoint, $params = [])
    {
            $response = self::getClient()->request('POST', $endpoint, ['json' => $params]);
            return json_decode($response->getBody()->getContents(), true);
    }
    public static function delete($endpoint, $params = [])
    {
            $response = self::getClient()->request('DELETE', $endpoint, ['query' => $params]);
            return json_decode($response->getBody()->getContents(), true);
    }
}
