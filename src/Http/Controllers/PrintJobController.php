<?php

namespace D3x\PrintNode\Http\Controllers;

use D3x\PrintNode\Models\PrintJob;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use Exception;
use Illuminate\Http\Request;


class PrintJobController extends Controller
{


    public function find(Request $request)
    {
        try {

            $printers = $request->printers ?? [];
            $jobs = $request->jobs ?? [];
            $print_jobs = PrintJob::get($printers, $jobs);
            return response()->success(SUCC::QUERY_MSG, $print_jobs);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }



    public function add(Request $request){
        try {
            $printer_id = $request->printer_id;
            $content_type = $request->content_type;
            $content = $request->content;

            $job = new PrintJob($printer_id, $content_type, $content);
            $job->post();

            return response()->success(SUCC::QUERY_MSG);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }
}