<?php

namespace D3x\PrintNode\Http\Controllers;

use D3x\PrintNode\Http\Controllers\Controller;
use D3x\PrintNode\Models\Computer;
use D3x\PrintNode\Models\Printer;
use Exception;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use Illuminate\Support\Facades\Request;

class ComputerController extends Controller
{
    public function find(Request $request)
    {
        try {
            $ids = $request->ids ?? [];
            $computers = Computer::get($ids);
            return response()->success(SUCC::QUERY_MSG, $computers);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function computerPrinters($computer_id, $printer_id = null)
    {
        try {
            $printers = Printer::get([$computer_id], []);
            return response()->success(SUCC::QUERY_MSG, $printers);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


}