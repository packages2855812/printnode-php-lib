<?php

use D3x\PrintNode\Http\Controllers\ComputerController;
use D3x\PrintNode\Http\Controllers\PrintJobController;
use Illuminate\Support\Facades\Route;

# ==> print-node <== #
Route::group(['prefix' => 'print-node', "middleware" => "auth:sanctum"], function () {

    # ==> computer <== #
    Route::group(['prefix' => 'computer'], function () {
        #------------------- DEFAULT --------------------#
        Route::get('/', [ComputerController::class, "find"]);
        Route::get('/{computer_id}/printers/{printer_id?}', [ComputerController::class, "computerPrinters"]);
        Route::delete("/", [ComputerController::class, "delete"]);
        #------------------- CUSTOM ---------------------#
    });

    # ==> print-job <== #
    Route::group(['prefix'=>'print-job'], function(){
        #------------------- DEFAULT --------------------#
        Route::get('/find',[PrintJobController::class,"find"]);
        Route::post("/add", [PrintJobController::class, "add"]);

        #------------------- CUSTOM ---------------------#
    });


});

