<?php

namespace D3x\PrintNode\Models;


use D3x\PrintNode\API\PrintNode;

class Printer
{
    public const KEY = "printers";

    /**
     * Sends a GET request to PrintNode proxy and returns Computers incorporating optional IDs.
     *
     * This method constructs an endpoint using a base key and appends a string of
     * IDs, either as a single ID or a comma-separated list from an array of IDs.
     * A GET request is then made to this endpoint, and the response is returned.
     *
     * @param mixed $printers Optional; a single ID or an array of IDs to append to the endpoint.
     * @param mixed $computers Optional; a single ID or an array of IDs to append to the endpoint.
     * @return array The response from the API call.
     */
    public static function get($computers = [], $printers = []): array
    {
        $endpoint = EndpointHelper::constructEndpoint('computers', self::KEY, $computers, $printers);
        return PrintNode::get($endpoint);
    }

}