<?php

namespace D3x\PrintNode\Models;

use D3x\PrintNode\API\PrintNode;

class Computer
{
    public const KEY = "computers";
    public static function get($computers = []): array
    {
        $endpoint = EndpointHelper::constructEndpoint('', self::KEY, [], $computers);
        return PrintNode::get($endpoint);
    }

    public static function delete($computers = []): array
    {
        $endpoint = EndpointHelper::constructEndpoint('', self::KEY, [], $computers);
        return PrintNode::delete($endpoint);
    }


}
