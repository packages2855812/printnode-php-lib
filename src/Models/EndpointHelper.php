<?php

namespace D3x\PrintNode\Models;
class EndpointHelper
{
    /**
     * Class EndpointHelper
     *
     * This class provides a method to construct an endpoint URL based on the given parameters.
     *
     * Usage:
     * The `constructEndpoint` method is used to create a formatted endpoint URL by combining
     * the type, key, and optional IDs. This helps in creating consistent and reusable endpoint
     * URLs for API requests.
     *
     * Method:
     * - public static function constructEndpoint(string $type, string $key, array $primaryIds = [], array $secondaryIds = []): string
     *
     * Parameters:
     * - string $type: The type of resource (e.g., 'printers', 'computers').
     * - string $key: The unique key associated with the request (e.g., 'your-printer-key').
     * - array $primaryIds: Optional array of primary IDs to include in the endpoint URL.
     * - array $secondaryIds: Optional array of secondary IDs to include in the endpoint URL.
     *
     * Returns:
     * - string: A formatted endpoint URL.
     *
     * Example Usage:
     *
     * // Construct an endpoint for printers with specific printer IDs and job IDs
     * $endpoint = EndpointHelper::constructEndpoint('printers', 'your-printer-key', [1, 2, 3], [101, 102]);
     * // Result: /printers/1,2,3/your-printer-key/101,102
     *
     * // Construct an endpoint for computers with specific computer IDs and printer IDs
     * $endpoint = EndpointHelper::constructEndpoint('computers', 'your-computer-key', [4, 5], [201, 202]);
     * // Result: /computers/4,5/your-computer-key/201,202
     *
     * // Construct an endpoint with no IDs
     * $endpoint = EndpointHelper::constructEndpoint('printers', 'your-printer-key');
     * // Result: /your-printer-key
     */

    public static function constructEndpoint(string $type, string $key, array $primaryIds = [], array $secondaryIds = []): string
    {
        $endpoint = "";
        if ($primaryIds) {
            $endpoint .= "/{$type}" . (empty($primaryIds) ? '' : '/' . implode(",", $primaryIds));
        }

        $endpoint .= "/{$key}" . (empty($secondaryIds) ? '' : '/' . implode(",", $secondaryIds));

        return $endpoint;
    }
}
