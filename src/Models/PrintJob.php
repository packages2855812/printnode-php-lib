<?php

namespace D3x\PrintNode\Models;

use D3x\PrintNode\API\PrintNode;

class PrintJob
{
    public const KEY = "printjobs";
    protected $printerId;
    protected $contentType;
    protected $content;
    protected $title;
    protected $source;
    protected $options;
    protected $expireAfter;
    protected $qty;
    protected $authentication;

    public function __construct($printerId, $contentType, $content)
    {
        $this->printerId = $printerId;
        $this->contentType = $contentType;
        $this->content = $content;
        $this->title = null;
        $this->source = null;
        $this->options = null;
        $this->expireAfter = null;
        $this->qty = 1; // default value is 1
        $this->authentication = null;
    }

    public function setOptions(array $options)
    {
        if (isset($options['title'])) {
            $this->title = $options['title'];
        }

        if (isset($options['source'])) {
            $this->source = $options['source'];
        }

        if (isset($options['options'])) {
            $this->options = $options['options'];
        }

        if (isset($options['expireAfter'])) {
            $this->expireAfter = $options['expireAfter'];
        }

        if (isset($options['qty'])) {
            $this->qty = $options['qty'];
        }

        if (isset($options['authentication'])) {
            $this->authentication = $options['authentication'];
        }
    }

    public static function get($printers = [], $jobs = []): array
    {
        $endpoint = EndpointHelper::constructEndpoint('printers', self::KEY, $printers, $jobs);
        return PrintNode::get($endpoint);
    }


    public function post()
    {
        $params = [
            'printerId' => $this->printerId,
            'contentType' => $this->contentType,
            'content' => $this->content,
        ];

        // Conditionally add parameters only if they are not null
        if ($this->title !== null) {
            $params['title'] = $this->title;
        }
        if ($this->source !== null) {
            $params['source'] = $this->source;
        }
        if ($this->options !== null) {
            $params['options'] = $this->options;
        }
        if ($this->expireAfter !== null) {
            $params['expireAfter'] = $this->expireAfter;
        }
        if ($this->qty !== null) {
            $params['qty'] = $this->qty;
        }
        if ($this->authentication !== null) {
            $params['authentication'] = $this->authentication;
        }

        return PrintNode::post('/printjobs', $params);
    }

    public static function delete($printers = [], $jobs = []): array
    {
        $endpoint = EndpointHelper::constructEndpoint('printers', self::KEY, $printers, $jobs);
        return PrintNode::delete($endpoint);
    }


}