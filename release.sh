#!/bin/bash

# Ensure we're in the repo directory, adjust as needed
cd $(dirname $0)

# Fetch tags and their info
git fetch --tags

# Default to initial version if no tags exist
if ! git describe --tags --abbrev=0 --always; then
  echo "No tags found. Setting initial tag to v0.1.0"
  newTag="v0.1.0"
else
  # Get the latest tag version
  latestTag=$(git describe --tags --abbrev=0)

  # If latestTag is not a valid SemVer, set initial version
  if ! [[ $latestTag =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "Latest tag is not in SemVer format. Setting initial tag to v0.1.0"
    newTag="v0.1.0"
  else
    # Split the version number into an array
    IFS='.' read -ra ADDR <<< "${latestTag#v}"

    # Increment the patch version
    newPatchVersion=$((ADDR[2] + 1))

    # Create a new version tag
    newTag="v${ADDR[0]}.${ADDR[1]}.$newPatchVersion"
  fi
fi

echo "New tag: $newTag"

# Create the new tag locally
git tag $newTag

# Push the new tag to the remote
git push origin $newTag
